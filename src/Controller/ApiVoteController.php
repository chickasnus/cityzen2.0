<?php

namespace App\Controller;

use App\Entity\Solution;
use App\Entity\Voted;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApiVoteController extends AbstractController
{
    /**
     * @Route("/vote/{id}", name="api_vote")
     * @param Solution $solution
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function index(Solution $solution,  EntityManagerInterface $manager)
    {
        // check if user is auth
        // TODO verifier que l'utilisateur est connecter pour voter (meme si il peux pas acceder a la page sans etre co ? error manque l'entiter user des la premiere ligne)
        if($this->isGranted($this->getUser())){
            $response= ['Etat_vote' => 500,'id_soluce'=>$solution->getId(), 'count_vote'=>$solution->getVoteds()->count()];
            $json = json_encode($response);
            return new Response($json,200);
        }

        $customer_try_vote = $this->getUser()->getCustomer();

        //test if this customer is in the list of the vote for this solution
        if($solution->userHasVoted($customer_try_vote)){

            $respon_json = ['Etat_vote' => 404,'id_soluce'=>$solution->getId(), 'count_vote'=>$solution->getVoteds()->count()-1];
            $voted = $solution->userVote($customer_try_vote);
            $manager->remove($voted);
            $manager->flush();
            $json = json_encode($respon_json);
            return new Response($json, 200);
        };
        $voted = new Voted();
        $new_vote = $voted
                        ->setSolution($solution)
                        ->setCreatedAt(new \DateTime())
                        ->setCustomer($customer_try_vote)
                        ;
        $manager->persist($new_vote);
        $manager->flush();

        $valide = ['Etat_vote' => 200,'id_soluce'=>$solution->getId(),'count_vote'=>$solution->getVoteds()->count()+1];
        $json = json_encode($valide);
        return new Response($json, 200);
    }
}
