<?php

namespace App\Controller;

use App\Entity\Problematic;
use App\Form\ProblematicFormType;
use App\Repository\ProblematicRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProblematicController extends AbstractController
{
    /**
     * @Route("/problematic/new", name="problematic_create")
     */
    public function create(Request $request,EntityManagerInterface $em)
    {
        $problematic = new Problematic();
        $form = $this->createForm(ProblematicFormType::class, $problematic);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $problematic = $form->getData();
            $em->persist($problematic);
            $em->flush();
        }

        return $this->render('problematic/edit.html.twig', [
            'form' => $form->createView(), 
        ]);
    }

    /**
     * @Route("/problematic/{id}/edit", name="problematic_edit")
     */
    public function update(Request $request,EntityManagerInterface $em, Problematic $problematic)
    {
        $form = $this->createForm(ProblematicFormType::class, $problematic);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $problematic = $form->getData();
            $em->flush();
        }

        return $this->render('problematic/edit.html.twig', [
            'form' => $form->createView(), 
        ]);
    }

    /**
     * @Route("/problematic", name="problematics")
     */
    public function index(ProblematicRepository $repo)
    {
        $problematics = $repo->findAll();
        return $this->render('problematic/index.html.twig', [
            'problematics' => $problematics,
        ]);
    }

    /**
     * @Route("/problematic/{id}", name="problematic_view")
     */
    public function view(Problematic $problematic)
    {
        return $this->render('problematic/view.html.twig', [
            'problematic' => $problematic,
        ]);
    }

    // /**
    //  * @Route("/problematic/{id}/delete", name="problematic_delete")
    //  */
    // public function delete(Problematic $problematic)
    // {
    //     return $this->render('problematic/view.html.twig', [
    //         'problematic' => $problematic,
    //     ]);
    // }

}
