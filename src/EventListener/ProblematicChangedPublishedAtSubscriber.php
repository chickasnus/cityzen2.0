<?php

namespace App\EventListener;

use Doctrine\ORM\Events;
use App\Entity\Problematic;
use Doctrine\Common\EventSubscriber;
use App\Repository\ProblematicRepository;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs as EventLifecycleEventArgs;
use Doctrine\ORM\EntityManagerInterface;

class ProblematicChangedPublishedAtSubscriber implements EventSubscriber
{
    public function getSubscribedEvents()
    {
        return [
            Events::postPersist,
            Events::postUpdate
        ];
    }

    public function postPersist(EventLifecycleEventArgs $args)
    {
        $problematic = $args->getObject();
        if($problematic instanceof Problematic)
        { 
            $problematic = $args->getObject();
            $repo = $args->getObjectManager()->getRepository(Problematic::class); 
            $em = $args->getObjectManager();

            $this->updateActualProbl($repo, $problematic, $em);
            $this->updateDateProbls($repo, $problematic, $em);
        }
    }
    
    public function postUpdate(EventLifecycleEventArgs $args)
    {
        $problematic = $args->getObject();
        if($problematic instanceof Problematic)
        { 
            $repo = $args->getObjectManager()->getRepository(Problematic::class); 
            $em = $args->getObjectManager();

            $this->updateActualProbl($repo, $problematic, $em);
            $this->updateDateProbls($repo, $problematic, $em);
        }   
    }
    

    private function updateDateProbls(ProblematicRepository $repo, Problematic $problematic, EntityManagerInterface $em)
    {
        $allNextProbl = $repo->findAllNextProbl($problematic->getPublishedAt());
        
        if(!empty($allNextProbl))
        {
            $nextDate = $problematic->getFinishedAt();
            foreach($allNextProbl as $problItem)
            {
                if($problItem->getPublishedAt() < $nextDate)
                {
                    $problItem->setPublishedAt($nextDate);
                    $nextDate = $problItem->getFinishedAt();
                }else{
                    break;
                }
            }
            $em->flush();
        }
    }

    private function updateActualProbl(ProblematicRepository $repo, Problematic $problematic, EntityManagerInterface $em)
    {
        $problCount = $repo->countElements();
        if($problCount === '0'){
            return;
        }
        $problBetween = $repo->findProblemBetween($problematic->getPublishedAt());
        if($problBetween !== null)
        {
            $diff = $problBetween->getPublishedAt()->diff($problematic->getPublishedAt());
            $problBetween
                ->setDurationValue($diff->days)
                ->setDurationUnit('day')
            ;
            $em->flush();         
        }
    }
}


