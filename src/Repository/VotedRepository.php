<?php

namespace App\Repository;

use App\Entity\Voted;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query\QueryException;
use Doctrine\Persistence\ManagerRegistry;
use http\Exception\BadMessageException;

/**
 * @method Voted|null find($id, $lockMode = null, $lockVersion = null)
 * @method Voted|null findOneBy(array $criteria, array $orderBy = null)
 * @method Voted[]    findAll()
 * @method Voted[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VotedRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Voted::class);
    }
     /**
      * @return Voted[] Returns an array of Voted objects
      */

   /* public function getOneVoteOneSoluce(Solution $solution, Customer $customer)
    {
            return $this->getEntityManager()->createQueryBuilder()
                ->select('v','c','s')
                ->from('App\Entity\voted','v')
                ->leftJoin('v.customer', 'c')
                ->andWhere('v.customer = :val')
                ->setParameter('val', $customer->getId())
                ->getQuery()
                ->getResult();
    }*/
    /*
    public function findOneBySomeField($value): ?Voted
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
