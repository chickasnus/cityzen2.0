<?php

namespace App\DataFixtures;

use App\Entity\Solution;
use App\Entity\Problematic;
use App\Entity\Customer;

use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;


class SolutionFixture extends BaseFixture implements DependentFixtureInterface
{
    public function getDependencies()
    {
        return [CustomerFixture::class, ProblematicFixture::class] ;
    }

    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(Solution::class, 5, function(Solution $solution, $i){
            $solution
                ->setCreatedAt(new \DateTime())
                ->setState('valide')
                ->setContent($this->faker->text)
                ->setCustomer($this->getReference(Customer::class.'_'.$i))
                ->setProblematic($this->getReference(Problematic::class.'_1'))
            ;
        });
        

        $manager->flush();
    }
}
