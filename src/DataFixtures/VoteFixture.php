<?php

namespace App\DataFixtures;


use App\Entity\Solution;
use App\Entity\Customer;
use App\Entity\Voted;


use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;


class VoteFixture extends BaseFixture implements DependentFixtureInterface
{
    //
    public function getDependencies()
    {
        return [CustomerFixture::class, SolutionFixture::class] ;
    }


    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(Voted::class, 5, function(Voted $voted, $i){
            $voted
                ->setCreatedAt(new \DateTime())
                ->setCustomer($this->getReference(Customer::class.'_'.$i))
                ->setSolution($this->getReference(Solution::class.'_'.$i))
            ;
        });
        $manager->flush();
    }
}
