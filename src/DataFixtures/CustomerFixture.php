<?php

namespace App\DataFixtures;

use App\Entity\Customer;
use App\Entity\User;

use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;


class CustomerFixture extends BaseFixture implements DependentFixtureInterface
{
    // method qui permet de loader l'ordre dans lequel on veut afficher les fixtures
    public function getDependencies()
    {
        return [UserFixture::class];
    }

    protected function loadData(ObjectManager $manager)
    {

        $this->createMany(Customer::class, 10, function(Customer $customer, $i){
            
            $customer
                ->setFirstName($this->faker->firstname)
                ->setLastName($this->faker->lastname)
                ->setState('blablabla')
                ->setPseudo($this->faker->userName)
                ->setUser($this->getReference(User::class.'_'.$i))
            ;   
        });

        $manager->flush();
    }  


}
