<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200609094912 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE customer (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, first_name VARCHAR(255) DEFAULT NULL, last_name VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, state VARCHAR(255) DEFAULT NULL, pseudo VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_81398E09A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE problematic (id INT AUTO_INCREMENT NOT NULL, customer_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, published_at DATETIME DEFAULT NULL, created_at DATETIME DEFAULT NULL, duration_value INT NOT NULL, duration_unit VARCHAR(255) NOT NULL, INDEX IDX_B366E46B9395C3F3 (customer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE solution (id INT AUTO_INCREMENT NOT NULL, problematic_id INT NOT NULL, customer_id INT NOT NULL, content LONGTEXT NOT NULL, created_at DATETIME NOT NULL, state VARCHAR(255) NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_9F3329DBBC0013C7 (problematic_id), INDEX IDX_9F3329DB9395C3F3 (customer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE upload (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE voted (id INT AUTO_INCREMENT NOT NULL, customer_id INT NOT NULL, solution_id INT NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_D258FF089395C3F3 (customer_id), INDEX IDX_D258FF081C0BE183 (solution_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE customer ADD CONSTRAINT FK_81398E09A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE problematic ADD CONSTRAINT FK_B366E46B9395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (id)');
        $this->addSql('ALTER TABLE solution ADD CONSTRAINT FK_9F3329DBBC0013C7 FOREIGN KEY (problematic_id) REFERENCES problematic (id)');
        $this->addSql('ALTER TABLE solution ADD CONSTRAINT FK_9F3329DB9395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (id)');
        $this->addSql('ALTER TABLE voted ADD CONSTRAINT FK_D258FF089395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (id)');
        $this->addSql('ALTER TABLE voted ADD CONSTRAINT FK_D258FF081C0BE183 FOREIGN KEY (solution_id) REFERENCES solution (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE problematic DROP FOREIGN KEY FK_B366E46B9395C3F3');
        $this->addSql('ALTER TABLE solution DROP FOREIGN KEY FK_9F3329DB9395C3F3');
        $this->addSql('ALTER TABLE voted DROP FOREIGN KEY FK_D258FF089395C3F3');
        $this->addSql('ALTER TABLE solution DROP FOREIGN KEY FK_9F3329DBBC0013C7');
        $this->addSql('ALTER TABLE voted DROP FOREIGN KEY FK_D258FF081C0BE183');
        $this->addSql('ALTER TABLE customer DROP FOREIGN KEY FK_81398E09A76ED395');
        $this->addSql('DROP TABLE customer');
        $this->addSql('DROP TABLE problematic');
        $this->addSql('DROP TABLE solution');
        $this->addSql('DROP TABLE upload');
        $this->addSql('DROP TABLE voted');
        $this->addSql('DROP TABLE user');
    }
}
