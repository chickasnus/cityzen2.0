<?php

namespace App\Entity;

use App\Repository\SolutionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SolutionRepository::class)
 */
class Solution
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $state;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity=Problematic::class, inversedBy="solutions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $problematic;

    /**
     * @ORM\ManyToOne(targetEntity=Customer::class, inversedBy="solutions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $customer;

    /**
     * @ORM\OneToMany(targetEntity=Voted::class, mappedBy="solution")
     */
    private $voteds;

    public function __construct()
    {
        $this->voteds = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getProblematic(): ?Problematic
    {
        return $this->problematic;
    }

    public function setProblematic(?Problematic $problematic): self
    {
        $this->problematic = $problematic;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return Collection|Voted[]
     */
    public function getVoteds(): Collection
    {
        return $this->voteds;
    }
    public function userHasVoted(Customer $customer){
        $arrayVoteds = $this->getVoteds();
        foreach ($arrayVoteds as $value){
            if($value->getCustomer()===$customer){
                return true;
            }
        }
        return false;
    }
    public function userVote(Customer $customer){
        $arrayVoteds = $this->getVoteds();
        foreach ($arrayVoteds as $value){
            if($value->getCustomer()===$customer){
                return $value;
            }
        }
        return false;
    }

    public function addVoted(Voted $voted): self
    {
        if (!$this->voteds->contains($voted)) {
            $this->voteds[] = $voted;
            $voted->setSolution($this);
        }

        return $this;
    }

    public function removeVoted(Voted $voted): self
    {
        if ($this->voteds->contains($voted)) {
            $this->voteds->removeElement($voted);
            // set the owning side to null (unless already changed)
            if ($voted->getSolution() === $this) {
                $voted->setSolution(null);
            }
        }

        return $this;
    }
}
